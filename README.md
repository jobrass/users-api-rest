# README #
Necessary steps to get the application up and running.
### How do I get set up? ###
#### Execute tests
```
#!bash
cd /to/project/path
mvn test
```

#### Compile jar
```
#!bash
cd /to/project/path
mvn clean compile assembly:single
```
#### Execute jar
```
#!bash
cd /to/project/path
java -jar target/webapp-1.0-SNAPSHOT-jar-with-dependencies.jar
```
#### Users available
id    | username | password | roles
----- | -------- | -------- | ------
admin | admin    | admin    | ADMIN
page1 | page1    | page1    | PAGE1
page2 | page2    | page2    | PAGE2
page3 | page3    | page3    | PAGE3

#### API REST Resource

 **GET** localhost:8000/user/{{id}}

 **POST** localhost:8000/user username={{username}}&password={{password}}&roles={{ROL1}},{{ROL2}},...

 **PUT** localhost:8000/user id={{id}}&username={{username}}&password={{password}}&roles={{ROL1}},{{ROL2}},...

 **DELETE** localhost:8000/user/{{id}}

#### Application
 * login:

      localhost:8000/web/home

 * Private resources:

      localhost:8000/web/page1

      localhost:8000/web/page2

      localhost:8000/web/page3