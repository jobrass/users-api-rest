package HttpDeliveryApp;


import UsersApp.Domain.User.UserPresenter;
import UsersApp.Domain.User.UserService;
import UsersApp.Infrastructure.Persitence.SqliteConfig;
import UsersApp.Infrastructure.Persitence.SqliteUserRepository;
import UsersApp.Infrastructure.Ui.PrivateUserWebPagePresenter;
import UsersApp.Infrastructure.Ui.RawUserPresenter;

public class Container {

    private SqliteUserRepository userRepository;
    private UserService userService;
    private UserPresenter userPresenter;
    private PrivateUserWebPagePresenter privateUserWebPagePresenter;

    public Container() {
        SqliteConfig config = new SqliteConfig(
            "jdbc:sqlite:"+System.getProperty("user.dir") + "/src/main/resources/users.db"
        );
        this.userRepository = new SqliteUserRepository(config);
        this.userService = new UserService(userRepository);
        this.userPresenter = new RawUserPresenter();
        this.privateUserWebPagePresenter = new PrivateUserWebPagePresenter();
    }

    public SqliteUserRepository getUserRepository() {
        return userRepository;
    }

    public UsersApp.Domain.User.UserService getUserService() {
        return userService;
    }

    public UserPresenter getUserPresenter() {
        return userPresenter;
    }

    public PrivateUserWebPagePresenter getPrivateUserWebPagePresenter() {
        return privateUserWebPagePresenter;
    }
}
