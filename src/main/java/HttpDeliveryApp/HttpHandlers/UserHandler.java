package HttpDeliveryApp.HttpHandlers;



import HttpDeliveryApp.Container;
import HttpDeliveryApp.HttpResponse;
import HttpDeliveryApp.HttpService;
import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserId;
import UsersApp.Domain.User.UserRequest;
import UsersApp.UseCase.CreateUser.CreateUserCommand;
import UsersApp.UseCase.CreateUser.CreateUserHandler;
import UsersApp.UseCase.CreateUser.CreateUserResponse;
import UsersApp.UseCase.DeleteUser.DeleteUserCommand;
import UsersApp.UseCase.DeleteUser.DeleteUserHandler;
import UsersApp.UseCase.GetUser.GetUserCommand;
import UsersApp.UseCase.GetUser.GetUserHandler;
import UsersApp.UseCase.UpdateUser.UpdateUserCommand;
import UsersApp.UseCase.UpdateUser.UpdateUserHandler;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserHandler implements HttpHandler {

    private Container container;

    private String regex = "/user/?([a-zA-Z0-9\\-]*)";

    public UserHandler(Container container) {
        this.container = container;
    }

    public void handle(HttpExchange httpExchange) throws IOException {

        HttpResponse response = new HttpResponse();

        if (httpExchange.getRequestURI().toString().matches(regex)) {
            try {
                response = this.getResponse(httpExchange);
            } catch (InvalidArgumentException e) {
                response.setBody(e.getMessage());
                response.setHttpCode(400);
            } catch (UnauthorizedException e) {
                response.setBody(e.getMessage());
                response.setHttpCode(401);
            } catch (ForbiddenException e) {
                response.setBody(e.getMessage());
                response.setHttpCode(403);
            } catch (UserNotFoundException e) {
                response.setBody(e.getMessage());
                response.setHttpCode(404);
            }
        } else {
            response.setBody("Not Found");
            response.setHttpCode(404);
        }

        httpExchange.sendResponseHeaders(response.getHttpCode(), response.getBody().length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBody().getBytes());
        os.close();
    }

    private HttpResponse getResponse(HttpExchange httpExchange)
            throws UnauthorizedException, UserNotFoundException, ForbiddenException, InvalidArgumentException,
            IOException {

        UserCredentials credentials = HttpService.httpBasicAuthentication(httpExchange);

        String method = httpExchange.getRequestMethod();

        if (method.equals("GET")) {
            return getUser(httpExchange, credentials);
        } else if (method.equals("POST")) {
            return createUser(httpExchange, credentials);
        } else if (method.equals("PUT")) {
            return updateUser(httpExchange, credentials);
        } else if (method.equals("DELETE")) {
            return deleteUser(httpExchange, credentials);
        }
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setHttpCode(404);
        httpResponse.setBody("Not found");

        return httpResponse;
    }

    private HttpResponse deleteUser(HttpExchange httpExchange, UserCredentials credentials)
            throws UnauthorizedException, ForbiddenException, InvalidArgumentException {

        HttpResponse httpResponse = new HttpResponse();
        DeleteUserCommand command = new DeleteUserCommand(credentials, new UserId(getUserId(httpExchange)));
        DeleteUserHandler useCase = new DeleteUserHandler(
                container.getUserService(),
                container.getUserRepository()
        );

        if (useCase.handle(command)) {
            httpResponse.setHttpCode(204);
        } else {
            httpResponse.setHttpCode(404);
            httpResponse.setBody("Not found");
        }

        return httpResponse;
    }

    private HttpResponse updateUser(HttpExchange httpExchange, UserCredentials credentials)
            throws IOException, UnauthorizedException, ForbiddenException, UserNotFoundException,
            InvalidArgumentException {
        Map<String, String> params = HttpService.getPostParams(httpExchange);

        HttpResponse httpResponse = new HttpResponse();

        if (params.get("id") == null ||
            params.get("username") == null ||
            params.get("password") == null ||
            params.get("roles") == null) {
            httpResponse.setBody("Missing some of this params: id,username,password,roles");
            httpResponse.setHttpCode(400);
            return httpResponse;
        }
        UserRequest userRequest = new UserRequest(
                new UserId(params.get("id")),
                params.get("username"),
                params.get("password"),
                params.get("roles")
        );

        UpdateUserCommand command = new UpdateUserCommand(credentials, userRequest);
        UpdateUserHandler useCase = new UpdateUserHandler(
                container.getUserService(),
                container.getUserRepository()
        );

        if (useCase.handle(command)) {
            httpResponse.setHttpCode(204);
        } else {
            httpResponse.setHttpCode(500);
            httpResponse.setBody("Something wrong happen");
        }
        return httpResponse;
    }

    private HttpResponse createUser(HttpExchange httpExchange, UserCredentials credentials)
            throws IOException, UnauthorizedException, ForbiddenException, InvalidArgumentException {

        HttpResponse httpResponse = new HttpResponse();

        Map<String, String> params = HttpService.getPostParams(httpExchange);

        if (params.get("username") == null ||
            params.get("password") == null ||
            params.get("roles") == null) {
            httpResponse.setBody("Missing username or password or roles params.");
            httpResponse.setHttpCode(400);
            return httpResponse;
        }
        UserRequest userRequest = new UserRequest(
                params.get("username"),
                params.get("password"),
                params.get("roles")
        );
        CreateUserCommand command = new CreateUserCommand(credentials,userRequest);
        CreateUserHandler useCase = new CreateUserHandler(
                container.getUserService(),
                container.getUserRepository()
        );
        CreateUserResponse response = useCase.handle(command);
        if (response.hasBeenSuccessul()) {
            httpResponse.setHttpCode(201);
            httpResponse.setBody(response.getUserId());
        } else {
            httpResponse.setHttpCode(500);
            httpResponse.setBody("Something wrong happen");
        }
        return httpResponse;
    }

    private HttpResponse getUser(HttpExchange httpExchange, UserCredentials credentials)
            throws UnauthorizedException, UserNotFoundException, InvalidArgumentException {

        HttpResponse httpResponse = new HttpResponse();
        GetUserCommand command = new GetUserCommand(credentials, new UserId(getUserId(httpExchange)));
        GetUserHandler useCase = new GetUserHandler(
                container.getUserService(),
                container.getUserRepository(),
                container.getUserPresenter()
        );
        httpResponse.setBody(useCase.handle(command));

        return httpResponse;
    }

    private String getUserId(HttpExchange httpExchange) throws InvalidArgumentException {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(httpExchange.getRequestURI().toString());
        matcher.find();
        String id = matcher.group(1);
        if (id.equals("")) {
            throw new InvalidArgumentException("Empty id");
        }
        return id;
    }
}
