package HttpDeliveryApp.HttpHandlers;


import HttpDeliveryApp.Container;
import HttpDeliveryApp.HttpResponse;
import HttpDeliveryApp.HttpService;
import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.Rol;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserId;
import UsersApp.UseCase.LogIn.LogInCommand;
import UsersApp.UseCase.LogIn.LogInHandler;
import UsersApp.UseCase.PrivateUserPage.PrivateUserPageCommand;
import UsersApp.UseCase.PrivateUserPage.PrivateUserPageHandler;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebHandler implements HttpHandler{

    private Container container;

    private String logInRegex = "^/web/home\\??(.*)$";
    private String logOutRegex = "^/web/logout$";
    private String privatePage = "^/web/page([123]{1})$";

    private String logInTemplate = "<!DOCTYPE html>"+
            "<html>" +
            "<head><title>LogInForm</title></head>"+
            "<body>"+
                "<form action=\"/web/home\" method=\"post\""+
                "<div>{{error}}</div>"+
                "<div>"+
                    "<label>Username: </label>"+
                    "<input type=\"text\" name=\"username\">"+
                "</div>"+
                "<div>"+
                    "<label>Password: </label>"+
                    "<input type=\"password\" name=\"password\">"+
                "</div>"+
                "<div>"+
                    "<input type=\"hidden\" name=\"resource\" value=\"{{resource}}\">"+
                    "<input type=\"submit\" value=\"SIGN IN\">"+
                "</div>"+
                "</form>"+
            "</body>"+
            "</html>";

    private String homeTemplate = "<!DOCTYPE html>"+
            "<html>" +
            "<head><title>Home</title></head>"+
            "<body>"+
                "<div><a href=\"/web/logout\">logout<a/></div>"+
                "<div>"+
                    "<ul>"+
                        "<li><a href=\"/web/page1\">page1</a></li>"+
                        "<li><a href=\"/web/page2\">page2</a></li>"+
                        "<li><a href=\"/web/page3\">page3</a></li>"+
                    "</ul>"+
                "</div>"+
            "</body>"+
            "</html>";
    private String privatePageTemplate = "<!DOCTYPE html>"+
            "<html>" +
            "<head><title>PrivatePage</title></head>"+
            "<body>"+
                "<div><a href=\"/web/logout\">logout<a/></div>"+
                "<div>{{msg}}</div>"+
            "</body>"+
            "</html>";


    public WebHandler(Container container) {
        this.container = container;
    }

    public void handle(HttpExchange httpExchange) throws IOException {

        HttpResponse response = new HttpResponse();

        String ssid = HttpService.getCookies(httpExchange).get("SSID");

        try {
            if (httpExchange.getRequestURI().toString().matches(logInRegex) && ssid == null) {
                response = getLoginResponse(httpExchange);
            } else if (httpExchange.getRequestURI().toString().matches(privatePage) && ssid == null) {
                response.setHttpCode(302);
                httpExchange.getResponseHeaders().add(
                        "location",
                        "/web/home?resource=" + URLEncoder.encode(httpExchange.getRequestURI().toString(), "UTF-8")
                );
            } else if (httpExchange.getRequestURI().toString().matches(logInRegex) && ssid != null) {
                response.setBody(this.homeTemplate);
            } else if (httpExchange.getRequestURI().toString().matches(privatePage) && ssid != null) {
                response = getPrivatePageResponse(httpExchange, ssid);
            } else if (httpExchange.getRequestURI().toString().matches(logOutRegex) && ssid != null) {
                response.setHttpCode(302);
                httpExchange.getResponseHeaders().add("location", "/web/home");
                removeSessionCookie(httpExchange);
            } else {
                response.setBody("Not Found");
                response.setHttpCode(404);
            }
        } catch (UserNotFoundException e) {
            response.setBody("Not Found");
            response.setHttpCode(404);
        } catch (ForbiddenException e) {
            response.setBody(e.getMessage());
            response.setHttpCode(403);
        }

        httpExchange.sendResponseHeaders(response.getHttpCode(), response.getBody().length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBody().getBytes());
        os.close();
    }

    private HttpResponse getPrivatePageResponse(HttpExchange httpExchange, String ssid) throws UserNotFoundException, ForbiddenException {
        HttpResponse httpResponse = new HttpResponse();
        Rol rol;
        Pattern pattern = Pattern.compile(privatePage);
        Matcher matcher = pattern.matcher(httpExchange.getRequestURI().toString());
        matcher.find();
        String privatePageMatch = matcher.group(1);
        if (privatePageMatch.equals("1")) {
            rol = Rol.PAGE_1;
        } else if (privatePageMatch.equals("2")) {
            rol = Rol.PAGE_2;
        } else {
            rol = Rol.PAGE_3;
        }

        PrivateUserPageCommand command = new PrivateUserPageCommand(new UserId(ssid), rol);
        PrivateUserPageHandler useCase = new PrivateUserPageHandler(
                container.getUserRepository(),
                container.getPrivateUserWebPagePresenter()
        );

        httpResponse.setBody(this.privatePageTemplate.replace("{{msg}}", useCase.handle(command)));

        return  httpResponse;
    }

    private HttpResponse getLoginResponse(HttpExchange httpExchange) throws IOException {

        String method = httpExchange.getRequestMethod();
        HttpResponse httpResponse = new HttpResponse();

        if (method.equals("GET")) {
            Pattern pattern = Pattern.compile(logInRegex);
            Matcher matcher = pattern.matcher(httpExchange.getRequestURI().toString());
            matcher.find();
            String query = matcher.group(1);
            String resource = HttpService.getQueryParams(query).get("resource");
            httpResponse.setBody(
                    this.logInTemplate
                            .replace("{{error}}","")
                            .replace("{{resource}}", resource == null ? "/web/home" : resource)
            );
        } else if (method.equals("POST")) {
            Map<String, String> params = HttpService.getPostParams(httpExchange);
            UserCredentials credentials = new UserCredentials(
                    params.get("username") == null ? "" : params.get("username"),
                    params.get("password") == null ? "" : params.get("password")
            );
            try {
                LogInCommand command = new LogInCommand(credentials);
                LogInHandler useCase = new LogInHandler(this.container.getUserService());
                httpResponse.setHttpCode(302);
                httpExchange.getResponseHeaders().add("location", params.get("resource"));
                setSessionCookie(useCase.handle(command).getId(), httpExchange, (60 * 5 * 1000));
            } catch (UnauthorizedException e) {
                httpResponse.setHttpCode(400);
                httpResponse.setBody(this.logInTemplate.replace("{{error}}",e.getMessage()));
            }
        } else {
            httpResponse.setHttpCode(404);
            httpResponse.setBody("Not found");
        }

        return httpResponse;
    }

    private void setSessionCookie(String id, HttpExchange httpExchange, int expireTime) {
        Date expdate= new Date();
        expdate.setTime(expdate.getTime() + expireTime);
        DateFormat df = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss zzz");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        httpExchange.getResponseHeaders().add(
                "Set-Cookie",
                "SSID="+id+"; " +
                "Domain=localhost; " +
                "Path=/; " +
                "Expires="+df.format(expdate)+"; " +
                "HttpOnly"
        );
    }
    private void removeSessionCookie(HttpExchange httpExchange) {

        httpExchange.getResponseHeaders().add(
                "Set-Cookie",
                "SSID=deleted; " +
                "Domain=localhost; " +
                "Path=/; " +
                "Expires=Thu, 01 Jan 1970 00:00:00 GMT; " +
                "HttpOnly"
        );
    }

}
