package HttpDeliveryApp;


public class HttpResponse {

    private int httpCode = 200;
    private String body = "";

    public int getHttpCode() {
        return httpCode;
    }

    public String getBody() {
        return body;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
