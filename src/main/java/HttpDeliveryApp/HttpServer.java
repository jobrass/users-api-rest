package HttpDeliveryApp;

import HttpDeliveryApp.HttpHandlers.UserHandler;
import HttpDeliveryApp.HttpHandlers.WebHandler;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.User.UserCredentials;
import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpContext;

import java.io.IOException;
import java.net.InetSocketAddress;


public class HttpServer {

    public static void start() {
        com.sun.net.httpserver.HttpServer server = null;
        final Container container = new Container();
        try {
            server = com.sun.net.httpserver.HttpServer.create(new InetSocketAddress(8000), 0);
            HttpContext userCtx = server.createContext("/user", new UserHandler(container));
            userCtx.setAuthenticator(new BasicAuthenticator("") {
                @Override
                public boolean checkCredentials(String username, String password) {
                    UserCredentials credentials = new UserCredentials(username, password);
                    try {
                        container.getUserService().SignIn(credentials);
                        return true;
                    } catch (UnauthorizedException e) {
                        return false;
                    }
                }
            });
            server.createContext("/web", new WebHandler(container));
            server.setExecutor(null);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
