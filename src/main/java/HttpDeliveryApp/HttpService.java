package HttpDeliveryApp;

import UsersApp.Domain.User.UserCredentials;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class HttpService {
    public static Map<String, String> getPostParams(HttpExchange httpExchange) throws IOException {

        String encoding = "UTF-8";
        String query;
        InputStream in = httpExchange.getRequestBody();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte buf[] = new byte[4096];
            for (int n = in.read(buf); n > 0; n = in.read(buf)) {
                out.write(buf, 0, n);
            }
            query = new String(out.toByteArray(), encoding);
        } finally {
            in.close();
        }

        return HttpService.getQueryParams(query);

    }

    public static UserCredentials httpBasicAuthentication(HttpExchange httpExchange)
            throws UnsupportedEncodingException {

        Headers headers = httpExchange.getRequestHeaders();
        String authorization = headers.getFirst("Authorization");

        String[] definition = authorization.split(" ");
        String decoded = new String(Base64.getDecoder().decode(definition[1]));
        String[] credentials = decoded.split(":");
        return new UserCredentials(credentials[0],credentials[1]);
    }

    public static Map<String, String> getCookies(HttpExchange httpExchange) {
        Headers headers = httpExchange.getRequestHeaders();
        String cookies = headers.getFirst("Cookie");
        Map<String,String> params = new HashMap<String,String>();
        if (cookies != null) {
            for (String cookie: cookies.split(";") ) {
                String[] param = cookie.split("=");
                if (param.length == 2) {
                    params.put(param[0].trim(), param[1].trim());
                }
            }
        }
        return params;
    }

    public static Map<String, String> getQueryParams(String query) {
        String encoding = "UTF-8";
        Map<String,String> params = new HashMap<String,String>();
        String defs[] = query.split("&");
        for (String def: defs) {
            String[] param = def.split("=");
            if (param.length == 2) {
                try {
                    params.put(
                            URLDecoder.decode(param[0], encoding),
                            URLDecoder.decode(param[1], encoding)
                    );
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return params;
    }
}
