package UsersApp.Domain;


import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserService;

public class AdminTaskCommand {

    private UserService service;
    private Rol[] allowedRol;

    public AdminTaskCommand(UserService service) {
        this.service = service;
        this.allowedRol = new Rol[1];
        this.allowedRol[0] = Rol.ADMIN;
    }

    public void isAuthorized(UserCredentials credentials)
            throws UnauthorizedException, ForbiddenException {
        User admin = service.SignIn(credentials);
        FirewallService.areAllowedRoles(admin.roles(), allowedRol);
    }
}
