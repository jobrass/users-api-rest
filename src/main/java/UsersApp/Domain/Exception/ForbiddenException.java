package UsersApp.Domain.Exception;

public class ForbiddenException extends Exception {
    public ForbiddenException(String message) {
        super(message);
    }
}
