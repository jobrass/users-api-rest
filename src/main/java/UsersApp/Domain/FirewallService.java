package UsersApp.Domain;

import UsersApp.Domain.Exception.ForbiddenException;

public class FirewallService {

    public static void areAllowedRoles(Rol[] userRoles, Rol[] allowedRoles) throws ForbiddenException {
        for (Rol userRol : userRoles) {
            if (FirewallService.isAllowedRol(userRol, allowedRoles)) {
                return;
            }
        }
        throw new ForbiddenException("Bad privileges");
    }

    private static boolean isAllowedRol(Rol rol, Rol[] allowedRoles) {
        for (Rol allowedRol: allowedRoles) {
            if (rol.equals(allowedRol)) {
                return true;
            }
        }
        return false;
    }
}
