package UsersApp.Domain;

public final class Rol {

    public static final Rol PAGE_1 = new Rol("PAGE_1");
    public static final Rol PAGE_2 = new Rol("PAGE_2");
    public static final Rol PAGE_3 = new Rol("PAGE_3");
    public static final Rol ADMIN = new Rol("ADMIN");

    private String rol;

    private Rol(String value) {
        rol = value;
    }

    @Override
    public String toString() {
        return rol;
    }

    public boolean equals(Rol rol) {
        return rol.toString() == this.rol;
    }
}
