package UsersApp.Domain;


import UsersApp.Domain.Exception.InvalidArgumentException;

public class RolService {

    public static Rol[] getRolesBy(String rolesString) throws InvalidArgumentException {
        String[] rolesArray = rolesString.split(",");
        Rol[] roles = new Rol[rolesArray.length];
        int i = 0;
        for (String rol : rolesArray) {
            roles[i] = RolService.getRolBy(rol);
            i++;
        }
        return roles;
    }

    private static Rol getRolBy(String rol) throws InvalidArgumentException {

        if (rol.equals(Rol.ADMIN.toString())) {
            return Rol.ADMIN;
        } else if (rol.equals(Rol.PAGE_1.toString())) {
            return Rol.PAGE_1;
        } else if (rol.equals(Rol.PAGE_2.toString())) {
            return Rol.PAGE_2;
        } else if (rol.equals(Rol.PAGE_3.toString())) {
            return Rol.PAGE_3;
        }

        throw new InvalidArgumentException("Invalid rol ".concat(rol));
    }

    public static String getRolesStringBy(Rol[] roles) {
        String rolesString = "";
        for (Rol rol: roles) {
            rolesString += rol.toString() + ",";
        }
        return rolesString.substring(0, rolesString.length()-1);
    }
}
