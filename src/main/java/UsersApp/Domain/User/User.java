package UsersApp.Domain.User;


import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Rol;

import java.util.Arrays;

public class User {
    private UserId id;
    private String name;
    private String password;
    private Rol[] roles;

    public User(UserId id, String name, String password, Rol[] roles) throws InvalidArgumentException {
        this.id = id;
        this.setName(name);
        this.setPassword(password);
        this.setRoles(roles);
    }

    public void setName(String name) throws InvalidArgumentException {
        if (name.isEmpty()) {
            throw new InvalidArgumentException("name must be filled");
        }
        this.name = name;
    }

    public void setPassword(String password) throws InvalidArgumentException {
        if (password.isEmpty()) {
            throw new InvalidArgumentException("Password must be filled");
        }
        this.password = password;
    }

    public void setRoles(Rol[] roles) throws InvalidArgumentException {
        if (roles.length == 0) {
            throw new InvalidArgumentException("At least one rol must be assigned");
        }
        this.roles = roles;
    }

    public UserId id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String password() {
        return password;
    }

    public Rol[] roles() {
        return roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!id.equals(user.id)) return false;
        if (!name.equals(user.name)) return false;
        if (!password.equals(user.password)) return false;

        return Arrays.equals(roles, user.roles);

    }
}
