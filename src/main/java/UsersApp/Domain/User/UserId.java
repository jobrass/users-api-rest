package UsersApp.Domain.User;

import java.util.UUID;

public class UserId {
    private String id;
    public UserId() {
        id = UUID.randomUUID().toString();
    }
    public UserId(String uuid) {
        id = uuid;
    }
    @Override
    public String toString() {
        return id;
    }

    public boolean equals(UserId id) {
        return this.id.equals(id.toString());
    }
}
