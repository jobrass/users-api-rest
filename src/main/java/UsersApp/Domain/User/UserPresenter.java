package UsersApp.Domain.User;

import UsersApp.Domain.User.User;

public interface UserPresenter {
    public String write(User user);
}
