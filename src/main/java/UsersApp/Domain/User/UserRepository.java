package UsersApp.Domain.User;

import UsersApp.Domain.Exception.UserNotFoundException;

public interface UserRepository {

    public User getUserBy(String username, String password) throws UserNotFoundException;

    public User getUserBy(UserId id) throws UserNotFoundException;

    public boolean save(User user);

    public boolean delete(UserId id);
}
