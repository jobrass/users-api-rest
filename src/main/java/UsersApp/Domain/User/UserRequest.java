package UsersApp.Domain.User;

public class UserRequest {

    private UserId id;
    private String username;
    private String password;
    private String roles;

    public UserRequest(String username, String password, String roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
    public UserRequest(UserId id, String username, String password, String roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public UserId getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRoles() {
        return roles;
    }
}
