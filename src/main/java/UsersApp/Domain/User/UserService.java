package UsersApp.Domain.User;

import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.Exception.UserNotFoundException;

public class UserService {
    private UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User SignIn(UserCredentials credentials) throws UnauthorizedException {
        try {
            return this.repository.getUserBy(credentials.getUsername(), credentials.getPassword());
        } catch (UserNotFoundException e) {
            throw new UnauthorizedException("Invalid username or password");
        }
    }
}
