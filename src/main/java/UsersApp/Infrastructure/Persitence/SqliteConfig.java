package UsersApp.Infrastructure.Persitence;

public class SqliteConfig {

    private String dbPath;

    public SqliteConfig(String dbPath) {
        this.dbPath = dbPath;
    }

    public String getDbPath() {
        return dbPath;
    }
}