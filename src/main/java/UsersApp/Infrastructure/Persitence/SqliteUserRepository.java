package UsersApp.Infrastructure.Persitence;


import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.RolService;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserId;
import UsersApp.Domain.User.UserRepository;

import java.sql.*;

public class SqliteUserRepository implements UserRepository{

    private Connection connection;
    private Statement statement;

    public SqliteUserRepository(SqliteConfig config) {
        try {
            connection = DriverManager.getConnection(config.getDbPath());
            statement = connection.createStatement();
        } catch(SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public User getUserBy(String username, String password) throws UserNotFoundException {
        return getUserBy(
            "SELECT * FROM users WHERE username='"+username+"' AND password='"+password+"'"
        );
    }

    public User getUserBy(UserId id) throws UserNotFoundException {
        return this.getUserBy("SELECT * FROM users WHERE id='"+id.toString()+"'");
    }

    private User getUserBy(String sql) throws UserNotFoundException {
        try {
            ResultSet result = statement.executeQuery(sql);
            return new User(
                    new UserId(result.getString("id")),
                    result.getString("username"),
                    result.getString("password"),
                    RolService.getRolesBy(result.getString("roles"))
            );
        } catch (SQLException e) {
            throw new UserNotFoundException("User not found");
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean save(User user) {
        int result;
        try {
            result = statement.executeUpdate(
                    "INSERT INTO users VALUES " +
                    "('"+user.id()+"'," +
                    "'"+user.name()+"'," +
                    "'"+user.password()+"'," +
                    "'"+RolService.getRolesStringBy(user.roles())+"')");

            return result == 1;
        } catch (SQLException e) {
            try {
                result = statement.executeUpdate(
                        "UPDATE users SET " +
                        "username='" + user.name() + "'," +
                        "password='" + user.password() + "'," +
                        "roles='" + RolService.getRolesStringBy(user.roles()) + "'" +
                        " WHERE id = '" + user.id() + "'"
                );
                return result == 1;
            } catch (SQLException e1) {
                e1.printStackTrace();
                return false;
            }
        }
    }

    public boolean delete(UserId id) {
        try {

            int result = statement.executeUpdate(
                    "DELETE FROM users WHERE id='"+ id.toString()+"'"
            );
            return result == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
