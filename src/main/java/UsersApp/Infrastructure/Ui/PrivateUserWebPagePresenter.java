package UsersApp.Infrastructure.Ui;


import UsersApp.Domain.User.User;
import UsersApp.UseCase.PrivateUserPage.PrivateUserPagePresenter;

public class PrivateUserWebPagePresenter implements PrivateUserPagePresenter{
    public String write(User user) {
        return "Hello "+user.name();
    }
}
