package UsersApp.Infrastructure.Ui;

import UsersApp.Domain.Rol;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserPresenter;

public class RawUserPresenter implements UserPresenter {
    public String write(User user) {

        String info = "UserId: " + user.id() +", Name: " + user.name() + ", Roles: ";
        for (Rol rol: user.roles()) {
            info += rol.toString() + ",";
        }
        return info.substring(0, info.length()-1);
    }
}
