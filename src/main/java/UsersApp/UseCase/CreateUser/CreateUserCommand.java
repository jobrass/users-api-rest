package UsersApp.UseCase.CreateUser;

import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserRequest;

public class CreateUserCommand {

    private UserCredentials credentials;
    private UserRequest user;

    public CreateUserCommand(UserCredentials credentials, UserRequest user) {
        this.credentials = credentials;
        this.user = user;
    }

    public UserCredentials getCredentials() {
        return credentials;
    }

    public UserRequest getUser() {
        return user;
    }
}
