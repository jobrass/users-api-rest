package UsersApp.UseCase.CreateUser;


import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.RolService;
import UsersApp.Domain.User.*;
import UsersApp.Domain.AdminTaskCommand;

public class CreateUserHandler extends AdminTaskCommand {

    private UserRepository repository;

    public CreateUserHandler(UserService service, UserRepository repository) {
        super(service);
        this.repository = repository;
    }

    public CreateUserResponse handle(CreateUserCommand command)
            throws UnauthorizedException, ForbiddenException, InvalidArgumentException {
        this.isAuthorized(command.getCredentials());
        return this.createUserResponse(command.getUser());
    }

    private CreateUserResponse createUserResponse(UserRequest request)
            throws InvalidArgumentException {
        User user = new User(
                new UserId(),
                request.getUsername(),
                request.getPassword(),
                RolService.getRolesBy(
                        request.getRoles()
                )
        );
        Boolean success = this.repository.save(user);

        if (success) {
            return new CreateUserResponse(true, user.id().toString());
        } else {
            return new CreateUserResponse(false);
        }
    }
}
