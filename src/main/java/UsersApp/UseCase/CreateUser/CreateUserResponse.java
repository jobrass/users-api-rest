package UsersApp.UseCase.CreateUser;


public class CreateUserResponse {

    private Boolean success;
    private String userId;

    public CreateUserResponse(Boolean success, String userId) {
        this.success = success;
        this.userId = userId;
    }

    public CreateUserResponse(Boolean success) {
        this.success = success;
    }

    public Boolean hasBeenSuccessul() {
        return success;
    }

    public String getUserId() {
        return userId;
    }
}
