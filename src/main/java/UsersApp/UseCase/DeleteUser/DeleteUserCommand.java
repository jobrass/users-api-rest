package UsersApp.UseCase.DeleteUser;


import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserId;

public class DeleteUserCommand {

    private UserCredentials credentials;
    private UserId id;

    public DeleteUserCommand(UserCredentials credentials, UserId id) {
        this.credentials = credentials;
        this.id = id;
    }

    public UserCredentials getCredentials() {
        return credentials;
    }

    public UserId getId() {
        return id;
    }
}
