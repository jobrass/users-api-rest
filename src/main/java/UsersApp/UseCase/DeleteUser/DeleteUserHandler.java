package UsersApp.UseCase.DeleteUser;


import UsersApp.Domain.AdminTaskCommand;
import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserService;

public class DeleteUserHandler extends AdminTaskCommand {

    private UserRepository repository;

    public DeleteUserHandler(UserService service, UserRepository repository) {
        super(service);
        this.repository = repository;
    }


    public boolean handle(DeleteUserCommand command)
            throws UnauthorizedException, ForbiddenException {

        this.isAuthorized(command.getCredentials());
        return repository.delete(command.getId());
    }
}
