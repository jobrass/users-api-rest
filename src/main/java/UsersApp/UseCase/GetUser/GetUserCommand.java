package UsersApp.UseCase.GetUser;

import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserId;

public class GetUserCommand {
    private UserCredentials credentials;
    private UserId id;

    public GetUserCommand(UserCredentials credentials, UserId id) {
        this.credentials = credentials;
        this.id = id;
    }

    public UserCredentials getCredentials() {
        return credentials;
    }

    public UserId getId() {
        return id;
    }
}
