package UsersApp.UseCase.GetUser;


import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.User.UserPresenter;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserService;

public class GetUserHandler {

    private UserService service;
    private UserRepository repository;
    private UserPresenter presenter;

    public GetUserHandler(
            UserService service,
            UserRepository repository,
            UserPresenter presenter
    ) {
        this.service = service;
        this.repository = repository;
        this.presenter = presenter;
    }

    public String handle(GetUserCommand command)
            throws UnauthorizedException, UserNotFoundException {
        this.service.SignIn(command.getCredentials());
        return this.presenter.write(this.repository.getUserBy(command.getId()));
    }
}
