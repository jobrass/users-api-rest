package UsersApp.UseCase.LogIn;


import UsersApp.Domain.User.UserCredentials;

public class LogInCommand {
    private UserCredentials credentials;

    public LogInCommand(UserCredentials credentials) {
        this.credentials = credentials;
    }

    public UserCredentials getCredentials() {
        return credentials;
    }
}
