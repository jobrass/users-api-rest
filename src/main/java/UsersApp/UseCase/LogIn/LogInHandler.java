package UsersApp.UseCase.LogIn;


import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserService;

public class LogInHandler {

    private UserService service;

    public LogInHandler(UserService service) {
        this.service = service;
    }

    public LogInResponse handle(LogInCommand command) throws UnauthorizedException {
        User user = this.service.SignIn(command.getCredentials());
        return new LogInResponse(user.id().toString());
    }
}
