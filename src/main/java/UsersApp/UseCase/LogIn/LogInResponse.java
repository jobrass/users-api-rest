package UsersApp.UseCase.LogIn;


public class LogInResponse {
    private String id;

    public LogInResponse(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
