package UsersApp.UseCase.PrivateUserPage;

import UsersApp.Domain.Rol;
import UsersApp.Domain.User.UserId;

public class PrivateUserPageCommand {
    private UserId id;
    private Rol allowedRol;

    public PrivateUserPageCommand(UserId id, Rol allowedRol) {
        this.id = id;
        this.allowedRol = allowedRol;
    }

    public UserId getId() {
        return id;
    }

    public Rol[] getAllowedRol() {
        Rol[] roles = {this.allowedRol};
        return roles;
    }
}
