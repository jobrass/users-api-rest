package UsersApp.UseCase.PrivateUserPage;

import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.FirewallService;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserRepository;

public class PrivateUserPageHandler {

    private UserRepository repository;
    private PrivateUserPagePresenter presenter;

    public PrivateUserPageHandler(UserRepository repository, PrivateUserPagePresenter presenter) {
        this.repository = repository;
        this.presenter = presenter;
    }

    public String handle(PrivateUserPageCommand command)
            throws UserNotFoundException, ForbiddenException {
        User user = this.repository.getUserBy(command.getId());
        FirewallService.areAllowedRoles(user.roles(), command.getAllowedRol());
        return this.presenter.write(user);
    }
}
