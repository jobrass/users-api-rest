package UsersApp.UseCase.PrivateUserPage;

import UsersApp.Domain.User.User;

public interface PrivateUserPagePresenter {
    public String write(User user);
}
