package UsersApp.UseCase.UpdateUser;

import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserRequest;

public class UpdateUserCommand {

    private UserCredentials credentials;
    private UserRequest request;

    public UpdateUserCommand(UserCredentials credentials, UserRequest request) {
        this.credentials = credentials;
        this.request = request;
    }

    public UserCredentials getCredentials() {
        return credentials;
    }

    public UserRequest getRequest() {
        return request;
    }
}
