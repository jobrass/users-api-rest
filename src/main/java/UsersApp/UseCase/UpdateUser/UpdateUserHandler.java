package UsersApp.UseCase.UpdateUser;

import UsersApp.Domain.AdminTaskCommand;
import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.RolService;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserRequest;
import UsersApp.Domain.User.UserService;

public class UpdateUserHandler extends AdminTaskCommand{

    private UserRepository repository;

    public UpdateUserHandler(UserService service, UserRepository repository) {
        super(service);
        this.repository = repository;
    }

    public boolean handle(UpdateUserCommand command)
            throws UnauthorizedException, ForbiddenException, UserNotFoundException, InvalidArgumentException {
        this.isAuthorized(command.getCredentials());
        this.repository.getUserBy(command.getRequest().getId());
        return this.repository.save(this.getUserEntityBy(command.getRequest()));
    }

    private User getUserEntityBy(UserRequest request) throws InvalidArgumentException {
        return new User(
                request.getId(),
                request.getUsername(),
                request.getPassword(),
                RolService.getRolesBy(request.getRoles())
        );
    }
}
