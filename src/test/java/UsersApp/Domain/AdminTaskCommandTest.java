package UsersApp.Domain;

import UsersApp.Domain.Exception.ForbiddenException;
import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserService;
import UsersApp.Domain.User.StubUserRepository;
import org.junit.Before;
import org.junit.Test;

public class AdminTaskCommandTest {

    private UserService service;
    private AdminTaskCommand command;

    @Before
    public void setUp() throws Exception {
        UserRepository repository = new StubUserRepository();
        service = new UserService(repository);
        command = new AdminTaskCommand(service);
    }

    @Test(expected = UnauthorizedException.class)
    public void testUnauthorizedUser() throws Exception {
        UserCredentials credentials = new UserCredentials("Unauthorized", "password");
        command.isAuthorized(credentials);
    }

    @Test(expected = ForbiddenException.class)
    public void testUserWithoutCorrectPrivileges() throws Exception {
        UserCredentials credentials = new UserCredentials("page1", "page1");
        command.isAuthorized(credentials);
    }

}