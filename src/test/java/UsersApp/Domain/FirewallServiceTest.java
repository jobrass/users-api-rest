package UsersApp.Domain;

import UsersApp.Domain.Exception.ForbiddenException;
import org.junit.Test;


public class FirewallServiceTest {

    @Test
    public void testUserRolesAreAllowed() throws Exception {
        Rol[] userRoles = {Rol.ADMIN};
        Rol[] allowedRoles = {Rol.ADMIN};
        FirewallService.areAllowedRoles(userRoles, allowedRoles);
    }

    @Test(expected=ForbiddenException.class)
    public void testUserRolesAreNotAllowed() throws Exception {
        Rol[] userRoles = {Rol.PAGE_1};
        Rol[] allowedRoles = {Rol.ADMIN};
        FirewallService.areAllowedRoles(userRoles, allowedRoles);
    }
}