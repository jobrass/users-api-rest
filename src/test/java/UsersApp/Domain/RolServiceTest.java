package UsersApp.Domain;

import UsersApp.Domain.Exception.InvalidArgumentException;
import org.junit.Test;

import static org.junit.Assert.*;


public class RolServiceTest {

    @Test
    public void testGetRolesByString() throws Exception {
        Rol[] rolesExpected = {Rol.ADMIN, Rol.PAGE_1, Rol.PAGE_2, Rol.PAGE_3};
        String rolesString = "ADMIN,PAGE_1,PAGE_2,PAGE_3";
        Rol[] roles = RolService.getRolesBy(rolesString);
        for (int i=0; i<4; i++) {
            assertTrue(rolesExpected[i].equals(roles[i]));
        }
    }
    @Test(expected = InvalidArgumentException.class)
    public void testInvalidRolGettingRolesByString() throws Exception {
        String rolesString = "INVALID";
        RolService.getRolesBy(rolesString);
    }

    @Test
    public void testGetRolesStringByString() throws Exception {
        String expected = "ADMIN,PAGE_1,PAGE_2,PAGE_3";
        Rol[] roles = {Rol.ADMIN, Rol.PAGE_1, Rol.PAGE_2, Rol.PAGE_3};
        assertEquals(expected, RolService.getRolesStringBy(roles));
    }
}