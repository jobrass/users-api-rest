package UsersApp.Domain.User;


import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.Rol;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserId;
import UsersApp.Domain.User.UserRepository;



public class StubUserRepository implements UserRepository {

    private User[] users;

    public StubUserRepository() {
        Rol[] admin = {Rol.ADMIN};
        Rol[] page1 = {Rol.PAGE_1};
        Rol[] page2 = {Rol.PAGE_2};
        Rol[] page3 = {Rol.PAGE_3};
        this.users = new User[5];
        try {
            this.users[0] = new User(new UserId("admin"), "admin", "admin", admin);
            this.users[1] = new User(new UserId("page1"), "page1", "page1", page1);
            this.users[2] = new User(new UserId("page2"), "page2", "page2", page2);
            this.users[3] = new User(new UserId("page3"), "page3", "page3", page3);
            this.users[4] = new User(new UserId("update"), "page3", "page3", admin);
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

    public User getUserBy(String username, String password) throws UserNotFoundException {
        for (User user : this.users) {
            if (user.name().equals(username) && user.password().equals(password)) {
                return user;
            }
        }
        throw new UserNotFoundException("User not found");
    }

    public User getUserBy(UserId id) throws UserNotFoundException {
        for (User user : this.users) {
            if (user.id().equals(id)) {
                return user;
            }
        }
        throw new UserNotFoundException("User not found");
    }

    public boolean save(User user) {
        return user.name().equals("new") || user.id().toString().equals("update");
    }

    public boolean delete(UserId id) {
        return id.toString().equals("canBeDeleted");
    }
}
