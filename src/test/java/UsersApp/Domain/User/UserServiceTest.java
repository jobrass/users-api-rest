package UsersApp.Domain.User;

import UsersApp.Domain.Exception.UnauthorizedException;
import UsersApp.Domain.Rol;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class UserServiceTest {

    UserService service;
    UserRepository repository;

    @Before
    public void setUp() throws Exception {
        repository = this.getUserRepository();
        service = new UserService(this.getUserRepository());
    }

    private UserRepository getUserRepository() {
        return new StubUserRepository();
    }

    @Test
    public void testSuccessfulSignIn() throws Exception {
        Rol[] roles = {Rol.ADMIN};
        User user = new User(new UserId("admin"), "admin", "admin", roles);
        assertEquals(user, service.SignIn(new UserCredentials("admin", "admin")));
    }

    @Test(expected=UnauthorizedException.class)
    public void testSignInException() throws Exception {
        service.SignIn(new UserCredentials("", ""));
    }
}