package UsersApp.Domain.User;

import UsersApp.Domain.Exception.InvalidArgumentException;
import UsersApp.Domain.Rol;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    UserId id = new UserId();
    String name = "testName";
    String password = "password";
    Rol[] roles = {Rol.ADMIN, Rol.PAGE_1, Rol.PAGE_2, Rol.PAGE_3};

    @Test
    public void userCanBeInstantiated() throws InvalidArgumentException {
        User user = new User(id, name, password, roles);
        assertEquals(id, user.id());
        assertEquals(name, user.name());
        assertEquals(password, user.password());
        assertArrayEquals(roles, user.roles());
    }

    @Test(expected=InvalidArgumentException.class)
    public void testUserNameMustNotBeEmpty() throws InvalidArgumentException {
        new User(id, "", password, roles);
    }

    @Test(expected=InvalidArgumentException.class)
    public void testPasswordMustNotBeEmpty() throws InvalidArgumentException {
        new User(id, name, "", roles);
    }

    @Test(expected=InvalidArgumentException.class)
    public void testRolesMustNotBeEmpty() throws InvalidArgumentException {
        Rol[] emptyRoles = new Rol[0];
        new User(id, name, password, emptyRoles);
    }
}