package UsersApp.Infrastructure.Persitence;

import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.Rol;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static org.junit.Assert.*;


public class SqliteUserRepositoryTest {

    private String dbPath;
    private SqliteUserRepository repository;
    private Connection connection;
    private Statement statement;

    @Before
    public void setUp() throws Exception {
        dbPath = System.getProperty("user.dir") + "/src/main/resources/test.db";
        String dbConnection = "jdbc:sqlite:"+dbPath;
        File f = new File(dbPath);
        f.getParentFile().mkdirs();
        f.createNewFile();
        Class.forName("org.sqlite.JDBC");

        connection = DriverManager.getConnection(dbConnection);
        statement = connection.createStatement();
        statement.setQueryTimeout(5);

        statement.executeUpdate("CREATE TABLE \"users\" (\n" +
                "    \"id\" TEXT PRIMARY KEY NOT NULL,\n" +
                "    \"username\" TEXT UNIQUE NOT NULL,\n" +
                "    \"password\" TEXT NOT NULL,\n" +
                "    \"roles\" TEXT NOT NULL\n" +
                ")");

        repository = new SqliteUserRepository(new SqliteConfig(dbConnection));
    }

    @After
    public void tearDown() throws Exception {
        File f = new File(dbPath);
        f.delete();
    }

    @Test
    public void testGetUserByUsernameAndPassword() throws Exception {
        statement.executeUpdate("INSERT INTO users VALUES ('test','test','test','ADMIN')");
        Rol[] roles = {Rol.ADMIN};
        User userExpected = new User(new UserId("test"),"test","test", roles);
        assertEquals(userExpected, repository.getUserBy("test","test"));
    }
    @Test(expected = UserNotFoundException.class)
    public void testGetUserByUsernameAndPasswordNotFound() throws Exception {
        repository.getUserBy("","");
    }

    @Test
    public void testGetUserById() throws Exception {
        statement.executeUpdate("INSERT INTO users VALUES ('test','test','test','ADMIN')");
        Rol[] roles = {Rol.ADMIN};
        UserId id = new UserId("test");
        User userExpected = new User(id,"test","test", roles);
        assertEquals(userExpected, repository.getUserBy(id));
    }

    @Test
    public void testSaveNewUser() throws Exception {
        Rol[] roles = {Rol.ADMIN};
        User user = new User(new UserId("test"),"test","test", roles);
        assertTrue(repository.save(user));
    }
    @Test
    public void testUpdateOneUser() throws Exception {
        Rol[] roles = {Rol.ADMIN};
        User user = new User(new UserId(),"test","test", roles);
        assertTrue(repository.save(user));
        user.setName("testUpdate");
        assertTrue(repository.save(user));
    }

    @Test
    public void testDeletingExistentUser() throws Exception {
        statement.executeUpdate("INSERT INTO users VALUES ('test','test','test','ADMIN')");
        assertTrue(repository.delete(new UserId("test")));
    }

    @Test
    public void testDeletingNonExistentUser() throws Exception {
        assertFalse(repository.delete(new UserId("test")));
    }

}