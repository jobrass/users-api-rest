package UsersApp.Infrastructure.Ui;

import UsersApp.Domain.Rol;
import UsersApp.Domain.User.User;
import UsersApp.Domain.User.UserId;
import org.junit.Test;

import static org.junit.Assert.*;

public class RawUserPresenterTest {

    @Test
    public void testWrite() throws Exception {
        RawUserPresenter presenter = new RawUserPresenter();
        Rol[] roles = {Rol.ADMIN};
        User user = new User(new UserId("test"), "test", "test", roles);
        assertEquals("UserId: test, Name: test, Roles: ADMIN", presenter.write(user));
    }
}