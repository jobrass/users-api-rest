package UsersApp.UseCase.CreateUser;

import UsersApp.Domain.Rol;
import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserRequest;
import UsersApp.Domain.User.UserService;
import UsersApp.Domain.User.StubUserRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class CreateUserHandlerTest {

    private UserRepository repository;
    private UserService service;
    private CreateUserHandler useCase;

    @Before
    public void setUp() throws Exception {
        repository = new StubUserRepository();
        service = new UserService(repository);
        useCase = new CreateUserHandler(service, repository);
    }

    @Test
    public void testNewUserCanBeCreated() throws Exception {
        UserRequest user = this.getUserRequest("new", "new", Rol.PAGE_1.toString());
        UserCredentials credentials = new UserCredentials("admin", "admin");
        CreateUserCommand command = new CreateUserCommand(credentials, user);
        CreateUserResponse response = useCase.handle(command);
        assertTrue(response.hasBeenSuccessul());
    }
    @Test
    public void testNewUserCanNotBeCreated() throws Exception {
        UserRequest user = this.getUserRequest("bad", "bad", Rol.PAGE_1.toString());
        UserCredentials credentials = new UserCredentials("admin", "admin");
        CreateUserCommand command = new CreateUserCommand(credentials, user);
        CreateUserResponse response = useCase.handle(command);
        assertFalse(response.hasBeenSuccessul());
    }

    private UserRequest getUserRequest(String name, String password, String roles) {
        return new UserRequest(name, password, roles);
    }
}