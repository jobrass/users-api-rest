package UsersApp.UseCase.DeleteUser;

import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserId;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserService;
import UsersApp.Domain.User.StubUserRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class DeleteUserHandlerTest {

    private UserRepository repository;
    private UserService service;
    private DeleteUserHandler useCase;

    @Before
    public void setUp() throws Exception {
        repository = new StubUserRepository();
        service = new UserService(repository);
        useCase = new DeleteUserHandler(service, repository);
    }

    @Test
    public void deleteUser() throws Exception {
        UserCredentials credentials = new UserCredentials("admin","admin");
        DeleteUserCommand command = new DeleteUserCommand(credentials, new UserId("canBeDeleted"));
        assertTrue(useCase.handle(command));
    }
    @Test
    public void deleteNonExistentUser() throws Exception {
        UserCredentials credentials = new UserCredentials("admin","admin");
        DeleteUserCommand command = new DeleteUserCommand(credentials, new UserId());
        assertFalse(useCase.handle(command));
    }
}