package UsersApp.UseCase.GetUser;

import UsersApp.Domain.User.*;
import UsersApp.Infrastructure.Ui.RawUserPresenter;
import UsersApp.Domain.User.StubUserRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GetUserHandlerTest {

    private UserRepository repository;
    private UserService service;
    private UserPresenter presenter;
    private GetUserHandler useCase;

    @Before
    public void setUp() throws Exception {
        repository = new StubUserRepository();
        service = new UserService(repository);
        presenter = new RawUserPresenter();
        useCase = new GetUserHandler(service, repository, presenter);
    }

    @Test
    public void testHandle() throws Exception {
        UserCredentials credentials = new UserCredentials("page1", "page1");
        UserId id = new UserId("page1");
        GetUserCommand command = new GetUserCommand(credentials, id);
        assertEquals(
                "UserId: page1, Name: page1, Roles: PAGE_1",
                useCase.handle(command)
        );
    }
}