package UsersApp.UseCase.LogIn;

import UsersApp.Domain.User.StubUserRepository;
import UsersApp.Domain.User.UserCredentials;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Domain.User.UserService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class LogInHandlerTest {

    private UserRepository repository;
    private UserService service;
    private LogInHandler useCase;

    @Before
    public void setUp() throws Exception {
        repository = new StubUserRepository();
        service = new UserService(repository);
        useCase = new LogInHandler(service);
    }

    @Test
    public void testHandle() throws Exception {
        UserCredentials credentials = new UserCredentials("admin","admin");
        LogInCommand command = new LogInCommand(credentials);
        assertEquals("admin", useCase.handle(command).getId());
    }
}