package UsersApp.UseCase.PrivateUserPage;

import UsersApp.Domain.Rol;
import UsersApp.Domain.User.StubUserRepository;
import UsersApp.Domain.User.UserId;
import UsersApp.Domain.User.UserRepository;
import UsersApp.Infrastructure.Ui.PrivateUserWebPagePresenter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrivateUserPageHandlerTest {

    private UserRepository repository;
    private PrivateUserPagePresenter presenter;
    private PrivateUserPageHandler useCase;

    @Before
    public void setUp() throws Exception {
        repository = new StubUserRepository();
        presenter = new PrivateUserWebPagePresenter();
        useCase = new PrivateUserPageHandler(repository, presenter);
    }

    @Test
    public void testHandleWriteAllowedUser() throws Exception {
        PrivateUserPageCommand command = new PrivateUserPageCommand(new UserId("page1"), Rol.PAGE_1);
        assertEquals("Hello page1", useCase.handle(command));
    }
}