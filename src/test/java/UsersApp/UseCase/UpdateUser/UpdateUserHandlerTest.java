package UsersApp.UseCase.UpdateUser;

import UsersApp.Domain.Exception.UserNotFoundException;
import UsersApp.Domain.User.*;
import UsersApp.Domain.User.StubUserRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class UpdateUserHandlerTest {

    private UserRepository repository;
    private UserService service;
    private UpdateUserHandler useCase;

    @Before
    public void setUp() throws Exception {
        repository = new StubUserRepository();
        service = new UserService(repository);
        useCase = new UpdateUserHandler(service, repository);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateNonExistentUser() throws Exception {
        UserCredentials credentials = new UserCredentials("admin", "admin");
        UserRequest userRequest = new UserRequest(new UserId(), "", "", "");
        UpdateUserCommand command = new UpdateUserCommand(credentials, userRequest);
        useCase.handle(command);
    }

    @Test
    public void testUpdateUser() throws Exception {
        UserCredentials credentials = new UserCredentials("admin", "admin");
        UserRequest userRequest = new UserRequest(new UserId("update"), "t", "t", "ADMIN");
        UpdateUserCommand command = new UpdateUserCommand(credentials, userRequest);
        assertTrue(useCase.handle(command));
    }
}